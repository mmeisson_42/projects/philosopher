# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/08/28 14:28:00 by mmeisson          #+#    #+#              #
#    Updated: 2017/05/22 19:09:21 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			= philo

CC				= clang

CFLAGS			= -MD -Wall -Werror -Wextra -O2

SRCS_PATHS		= ./srcs/
SRCS			= main.c philosophers.c philosophers_init.c setters.c sticks.c \
				  sticks_locks.c tools.c

SRCS_UI_PATHS	= ./srcs_ui/
SRCS_UI			= draw.c init.c line.c line_cadran.c line_octant.c

INCS_PATHS		= ./incs/ ./libft/incs/ ./minilib/
INCS			= $(addprefix -I,$(INCS_PATHS))

OBJS_PATH		= ./.objs/
OBJS_NAME		= $(SRCS:.c=.o) $(SRCS_UI:.c=.o)
OBJS			= $(addprefix $(OBJS_PATH), $(OBJS_NAME))

DEPS			= $(OBJS:.o=.d)

LIB_PATHS		= ./minilib ./libft
LIBS			= $(addprefix -L,$(LIB_PATHS))

LDFLAGS			= $(LIBS) -lpthread -lmlx -lft -framework OpenGL -framework AppKit



all: $(NAME)


$(NAME): $(OBJS)
	@$(foreach PATHS, $(LIB_PATHS),\
		echo "# # # # # #\n#";\
		echo '# \033[31m' Compiling $(PATHS) '\033[0m';\
		echo "#\n# # # # # #";\
		make -j8 -C $(PATHS);\
	)
	$(CC) $^ -o $@ $(LDFLAGS)

$(OBJS_PATH)%.o: $(SRCS_UI_PATHS)%.c
	@mkdir -p $(OBJS_PATH)
	$(CC) $(CFLAGS) $(INCS) -o $@ -c $<

$(OBJS_PATH)%.o: $(SRCS_PATHS)%.c
	@mkdir -p $(OBJS_PATH)
	$(CC) $(CFLAGS) $(INCS) -o $@ -c $<

clean:
	@$(foreach PATHS, $(LIB_PATHS), make -j8 -C $(PATHS) clean;)
	rm -rf $(OBJS_PATH)

fclean:
	@$(foreach PATHS, $(LIB_PATHS), make -j8 -C $(PATHS) fclean;)
	rm -rf $(OBJS_PATH)
	rm -f $(NAME)

re: fclean all

-include $(DEPS)
