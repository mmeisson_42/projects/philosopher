/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/25 15:20:38 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/28 19:41:54 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <pthread.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdbool.h>
# include <assert.h>
# include <stdio.h>
# include <math.h>
# include "libft.h"
# include "mlx.h"

# define TIMEOUT 30
# define PHILO 7
# define LIFE 10
# define T_EAT 1
# define T_SLEEP 1
# define T_THINK 1

# define PI (M_PI)
# define WIDTH 900
# define HEIGHT (WIDTH)
# define MIDDLE (WIDTH / 2)

# define TO_USEC(sec) (useconds_t)(sec*1000000)

typedef enum	e_state
{
	NO_STATE,
	SLEEP,
	EAT,
	THINK,
}				t_state;

typedef struct	s_stick
{
	enum e_state		state;
	int					philosopher_id;
	pthread_mutex_t		lock;
}				t_stick;

typedef enum	e_side
{
	NONE,
	LEFT,
	RIGHT,
	BOTH,
}				t_side;

/*
**  Every struct s_philosophers.stick are going to point on the same
** struct s_stick data pointer
*/
typedef struct	s_philosopher
{
	int				id;
	int				life;
	pthread_mutex_t	lock;
	enum e_state	state;
	struct s_stick	*sticks;
	time_t			initial_timestamp;
	pthread_t		thread;
}				t_philosopher;

typedef struct	s_line
{
	int				x1;
	int				y1;
	int				x2;
	int				y2;
	int				color;
}				t_line;

typedef struct	s_poly
{
	int				r;
	int				n;
	int				midx;
	int				midy;
	int				color;
}				t_poly;

typedef struct	s_gui
{
	struct s_philosopher	*philosophers;
	void					*mlx;
	void					*win;
	void					*img;
	char					*px;
	int						bpp;
	int						sl;
	int						ed;
}				t_gui;

/*
** tools.c
*/
useconds_t		time_to_act(struct s_philosopher *this);
void			ft_bzero(void *ptr, size_t size);
bool			all_alive(struct s_philosopher *philosophers);

t_philosopher	*philosophers_init(void);

void			*cycles(void *this);

void			set_eating(struct s_philosopher *this);
void			set_sleeping(struct s_philosopher *this);
void			set_thinking(struct s_philosopher *this, enum e_side side);

/*
**  primitives
*/
void			lock_sticks(struct s_philosopher *this);
void			unlock_sticks(struct s_philosopher *this);

/*
** Followings 4 are thread safe by themself, using lock and unlock-sticks
*/
bool			stick_is_free(struct s_philosopher *this, t_side side);
void			free_sticks(struct s_philosopher *this);
bool			stolen_stick(struct s_philosopher *this);
int				which_sticks(struct s_philosopher *this);
void			take_stick(struct s_philosopher *this, t_side side,
		t_state eat);

/*
** UI's side
*/

/*
** x, y == coords
** c == color
** p == polygone
** l == line
** dx, dy == ?
*/

/*
** draw.c
*/
void			draw_gui(t_gui *e);
void			draw_table(t_gui *e);
void			draw_polygone(t_gui *e, t_poly p);
void			draw_plates(t_gui *e);
void			draw_lifes(t_gui *e);
void			draw_sticks(t_gui *e);
void			draw_middle_stick(t_gui *e, int i, int i2, int i3);
void			draw_stick(t_gui *e, int i, int color);
void			check_middle_left(t_gui *e, int i, int i2);
void			draw_circle(t_gui *e, t_poly p);
void			ft_px(t_gui *e, int x, int y, int c);

/*
** line.c
*/
void			ft_draw_line(t_gui *e, t_line l);
void			ft_draw_line2(t_gui *e, t_line l, int dx, int dy);
void			ft_draw_line3(t_gui *e, t_line l, int dx, int dy);

/*
** line_cadran.c
*/
void			ft_cadran1(t_gui *e, t_line l, int dx, int dy);
void			ft_cadran2(t_gui *e, t_line l, int dx, int dy);
void			ft_cadran3(t_gui *e, t_line l, int dx, int dy);
void			ft_cadran4(t_gui *e, t_line l, int dx, int dy);

/*
** line_octant.c
*/
void			ft_octant2(t_gui *e, t_line l, int dx, int dy);
void			ft_octant3(t_gui *e, t_line l, int dx, int dy);
void			ft_octant6(t_gui *e, t_line l, int dx, int dy);
void			ft_octant7(t_gui *e, t_line l, int dx, int dy);

/*
** init.c
*/
void			ft_init_gui(t_gui *e);

#endif
