/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_avldel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/28 17:24:55 by mmeisson          #+#    #+#             */
/*   Updated: 2016/08/28 17:28:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_avldel(t_avl **tree, void (*del)(void *, size_t))
{
	if (*tree != NULL)
	{
		ft_avldel(&(*tree)->left, del);
		ft_avldel(&(*tree)->right, del);
		ft_avldelnode(tree, del);
	}
}
