/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_avlrotate.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/28 16:37:10 by mmeisson          #+#    #+#             */
/*   Updated: 2016/08/28 16:58:20 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_avlrotatelr(t_avl **node)
{
	t_avl		*root;
	t_avl		*b;
	t_avl		*c;

	root = *node;
	b = root->left;
	c = b->right;
	root->left = c->right;
	b->right = c->left;
	c->left = b;
	c->right = root;
	*node = c;
}

void		ft_avlrotaterl(t_avl **node)
{
	t_avl		*root;
	t_avl		*b;
	t_avl		*c;

	root = *node;
	b = root->right;
	c = b->left;
	root->right = c->left;
	b->left = c->right;
	c->right = b;
	c->left = root;
	*node = c;
}

void		ft_avlrotatell(t_avl **node)
{
	t_avl		*root;
	t_avl		*b;

	root = *node;
	b = root->left;
	root->left = b->right;
	b->right = root;
	*node = b;
}

void		ft_avlrotaterr(t_avl **node)
{
	t_avl		*root;
	t_avl		*b;

	root = *node;
	b = root->right;
	root->right = b->left;
	b->left = root;
	*node = b;
}
