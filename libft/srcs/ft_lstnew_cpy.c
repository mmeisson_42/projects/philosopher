/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew_cpy.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/11 23:11:55 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/11 19:51:33 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list			*ft_lstnew_cpy(void *content, size_t content_size)
{
	t_list		*node;

	if (!(node = malloc(sizeof(*node))))
		return (NULL);
	node->content = content;
	node->content_size = content_size;
	return (node);
}
