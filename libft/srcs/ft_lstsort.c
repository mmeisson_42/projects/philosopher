/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/06 15:44:40 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/06 15:45:09 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstsort(t_list *lst, int (*cmp)(void*, void*, size_t))
{
	t_list	*tmp;
	t_list	*b_list;

	if (lst)
		tmp = ft_lstsort(lst->next, cmp);
	else
		return (NULL);
	if (!tmp)
		return (lst);
	if (cmp(lst->content, tmp->content, lst->content_size) <= 0)
	{
		lst->next = tmp;
		return (lst);
	}
	b_list = tmp;
	while (tmp->next &&
		cmp(lst->content, tmp->next->content, lst->content_size) > 0)
		tmp = tmp->next;
	lst->next = tmp->next;
	tmp->next = lst;
	return (b_list);
}
