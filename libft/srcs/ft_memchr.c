/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:59:50 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 16:01:59 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	*str;
	unsigned char	cbis;

	str = (unsigned char*)s;
	cbis = (unsigned char)c;
	while (n--)
	{
		if (*str == cbis)
			return (str);
		str++;
	}
	return (NULL);
}
