/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sortinsert.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/06 15:40:29 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/06 15:59:11 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_sortinsert(int *tab, size_t size)
{
	unsigned int	i;
	unsigned int	j;
	unsigned int	k;
	int				*tmp;

	i = 0;
	while (i < --size)
	{
		tmp = tab + i;
		j = i;
		while (++j <= size)
		{
			tmp = (tab[j] < *tmp) ? tab + j : tmp;
			if (j == size && tmp == tab + i)
				return ;
		}
		ft_swap(tmp, tab + i);
		k = size;
		tmp = tab + k;
		while (--k > i)
			if (tab[k] > *tmp)
				tmp = tab + k;
		ft_swap(tmp, tab + size);
		i++;
	}
}
