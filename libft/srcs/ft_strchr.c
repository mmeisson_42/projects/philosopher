/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:01:56 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 16:00:06 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	char	cbis;

	cbis = (char)c;
	while (*s || !c)
	{
		if (*s == cbis)
			return ((char*)s);
		s++;
	}
	return (NULL);
}
