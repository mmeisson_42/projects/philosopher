/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnover.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 20:56:24 by mmeisson          #+#    #+#             */
/*   Updated: 2016/04/27 03:05:10 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnover(char *erase, const char *str, size_t n)
{
	char	*n_str;
	int		i;
	int		j;

	i = (erase) ? ft_strlen(erase) : 0;
	j = (str) ? ft_strlen(str) : 0;
	n_str = ft_memalloc(sizeof(char) * (i + j + 1));
	if (n_str)
	{
		if (erase)
			ft_strcpy(n_str, erase);
		if (str)
			ft_strncpy(n_str + i, str, n);
		if (erase)
			free(erase);
	}
	return (n_str);
}
