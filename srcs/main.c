/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 17:38:50 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/25 17:45:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

int		main(void)
{
	struct s_philosopher	*philosophers;
	size_t					i;
	struct s_gui			e;

	i = 0;
	ft_bzero(&e, sizeof(e));
	if ((philosophers = philosophers_init()) == NULL)
		return (1);
	e.philosophers = philosophers;
	while (i < PHILO)
	{
		pthread_create(&philosophers[i].thread, NULL, cycles, &philosophers[i]);
		i++;
	}
	ft_init_gui(&e);
	mlx_loop(e.mlx);
	while (i < PHILO)
	{
		pthread_cancel(philosophers[i].thread);
		pthread_join(philosophers[i].thread, NULL);
		i++;
	}
	return (0);
}
