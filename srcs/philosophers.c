/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 18:35:32 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/28 19:36:53 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

/*
** The only action that may occur on this->life from an other thread
** is a read-only action from main thread. That's why checking
** this->life without lock is safe
*/

bool		cycle(struct s_philosopher *this)
{
	time_t	act;

	act = time_to_act(this);
	while (act-- > 0)
	{
		usleep(1000000);
		pthread_mutex_lock(&this->lock);
		if (this->state != EAT || 1)
			this->life--;
		pthread_mutex_unlock(&this->lock);
		if (this->life < 1)
			return (false);
		if (this->state == THINK && stolen_stick(this))
			return (true);
	}
	pthread_mutex_lock(&this->lock);
	if (this->state == EAT)
		this->life = LIFE;
	pthread_mutex_unlock(&this->lock);
	return (true);
}

static void	set_states(struct s_philosopher *this, bool left, bool right)
{
	if (this->state == EAT)
		set_thinking(this, LEFT);
	else if (this->state && left && right && this->life < LIFE)
		set_eating(this);
	else if (this->state != THINK && left)
		set_thinking(this, LEFT);
	else if (this->state != THINK && right)
		set_thinking(this, RIGHT);
	else
		set_sleeping(this);
}

void		*cycles(void *arg)
{
	struct s_philosopher	*this;
	bool					left;
	bool					right;
	bool					cont;

	this = (struct s_philosopher *)arg;
	while (time(NULL) - this->initial_timestamp < TIMEOUT && this->life > 0)
	{
		pthread_mutex_lock(&this->lock);
		lock_sticks(this);
		left = stick_is_free(this, LEFT);
		right = stick_is_free(this, RIGHT);
		set_states(this, left, right);
		pthread_mutex_unlock(&this->lock);
		unlock_sticks(this);
		cont = cycle(this);
		lock_sticks(this);
		free_sticks(this);
		unlock_sticks(this);
		if (cont == false)
			break ;
	}
	return (this->life > 0) ? (arg) : (NULL);
}
