/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers_init.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/25 20:30:43 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/25 20:32:54 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

struct s_philosopher	*philosophers_init(void)
{
	struct s_philosopher	*philosophers;
	struct s_stick			*sticks;
	time_t					timestamp;
	size_t					i;

	i = 0;
	timestamp = time(NULL);
	philosophers = ft_memalloc(sizeof(struct s_philosopher) * PHILO);
	sticks = ft_memalloc(sizeof(struct s_stick) * PHILO);
	if (philosophers == NULL || sticks == NULL)
		return (NULL);
	while (i < PHILO)
	{
		philosophers[i].state = NO_STATE;
		philosophers[i].sticks = sticks;
		philosophers[i].life = LIFE;
		philosophers[i].id = i;
		philosophers[i].initial_timestamp = timestamp;
		pthread_mutex_init(&philosophers[i].lock, NULL);
		pthread_mutex_init(&sticks[i].lock, NULL);
		sticks[i].state = NO_STATE;
		sticks[i].philosopher_id = -1;
		i++;
	}
	return (philosophers);
}
