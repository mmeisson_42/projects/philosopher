/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setters.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/22 19:56:35 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/25 17:44:28 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void			set_eating(struct s_philosopher *this)
{
	this->state = EAT;
	take_stick(this, LEFT, EAT);
	take_stick(this, RIGHT, EAT);
}

void			set_sleeping(struct s_philosopher *this)
{
	this->state = SLEEP;
}

void			set_thinking(struct s_philosopher *this, t_side side)
{
	this->state = THINK;
	take_stick(this, side, THINK);
}
