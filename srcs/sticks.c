/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sticks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 18:29:29 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/25 20:33:21 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

/*
** These functions are thread safe by themselves
*/

bool			stick_is_free(struct s_philosopher *this, t_side side)
{
	bool	res;
	int		ind;

	ind = (side == LEFT) ? 0 : 1;
	res = (this->sticks[(this->id + ind) % PHILO].philosopher_id == -1) ||
		((this->life < LIFE / 2) &&
		this->sticks[(this->id + ind) % PHILO].state == THINK);
	return (res);
}

void			free_sticks(struct s_philosopher *this)
{
	if (this->sticks[this->id].philosopher_id == this->id)
	{
		this->sticks[this->id].state = NO_STATE;
		this->sticks[this->id].philosopher_id = -1;
	}
	if (this->sticks[this->id + 1].philosopher_id == this->id)
	{
		this->sticks[(this->id + 1) % PHILO].state = NO_STATE;
		this->sticks[(this->id + 1) % PHILO].philosopher_id = -1;
	}
}

void			take_stick(struct s_philosopher *this, t_side side,
		t_state state)
{
	int		ind;

	ind = (side == LEFT) ? 0 : 1;
	this->sticks[(this->id + ind) % PHILO].state = state;
	this->sticks[(this->id + ind) % PHILO].philosopher_id = this->id;
}

bool			stolen_stick(struct s_philosopher *this)
{
	bool	res;

	lock_sticks(this);
	res = (this->sticks[this->id].philosopher_id != this->id &&
			this->sticks[(this->id + 1) % PHILO].philosopher_id != this->id);
	unlock_sticks(this);
	return (res);
}

int				which_sticks(struct s_philosopher *this)
{
	int		res;

	res = 0;
	lock_sticks(this);
	if (this->sticks[this->id].philosopher_id == this->id)
		res += 1;
	if (this->sticks[(this->id + 1) % PHILO].philosopher_id == this->id)
		res += 2;
	unlock_sticks(this);
	return (res);
}
