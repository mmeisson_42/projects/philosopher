/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sticks_locks.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/25 18:43:29 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/25 18:43:32 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void		lock_sticks(struct s_philosopher *this)
{
	if (pthread_mutex_lock(&this->sticks[this->id].lock))
		perror("lock LEFT");
	if (pthread_mutex_lock(&this->sticks[(this->id + 1) % PHILO].lock))
		perror("lock RIGHT");
}

void		unlock_sticks(struct s_philosopher *this)
{
	if (pthread_mutex_unlock(&this->sticks[this->id].lock))
		perror("unlock LEFT");
	if (pthread_mutex_unlock(&this->sticks[(this->id + 1) % PHILO].lock))
		perror("unlock RIGHT");
}
