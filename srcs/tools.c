/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/25 20:33:36 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/25 20:34:48 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

useconds_t	time_to_act(struct s_philosopher *this)
{
	time_t	max_time;
	time_t	required_time;

	max_time = this->initial_timestamp - TIMEOUT;
	if (this->state == SLEEP)
		required_time = T_SLEEP;
	else if (this->state == EAT)
		required_time = T_EAT;
	else
		required_time = T_THINK;
	return (max_time > required_time) ? (required_time) : (max_time);
}

void		ft_bzero(void *ptr, size_t size)
{
	size_t			i;
	unsigned char	*p;

	i = 0;
	p = (unsigned char *)ptr;
	while (i < size)
	{
		p[i] = 0;
		i++;
	}
}

bool		all_alive(struct s_philosopher *philosophers)
{
	size_t		i;
	bool		res;

	i = 0;
	res = true;
	while (i < PHILO)
	{
		pthread_mutex_lock(&philosophers[i].lock);
		if (philosophers[i].life < 1)
		{
			res = false;
		}
		pthread_mutex_unlock(&philosophers[i].lock);
		i++;
	}
	return (res);
}
