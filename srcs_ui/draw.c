/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cemonet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 16:36:31 by cemonet           #+#    #+#             */
/*   Updated: 2017/05/25 16:42:12 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void	ft_px(t_gui *e, int x, int y, int c)
{
	int		r;
	int		g;
	int		b;

	r = (c & 0xFF0000) >> 16;
	g = (c & 0xFF00) >> 8;
	b = (c & 0xFF);
	if (y >= 0 && x >= 0 && y < HEIGHT && x < WIDTH)
	{
		e->px[(y * e->sl) + ((e->bpp / 8) * x) + 2] = r;
		e->px[(y * e->sl) + ((e->bpp / 8) * x) + 1] = g;
		e->px[(y * e->sl) + ((e->bpp / 8) * x)] = b;
	}
}

void draw_gui(t_gui *e)
{
	draw_table(e);
	draw_plates(e);
	draw_sticks(e);
}

void draw_table(t_gui *e)
{
	t_poly	p;
	size_t	i;

	i = 0;
	p.r = WIDTH / 3;
	p.n = PHILO;
	p.midx = MIDDLE;
	p.midy = MIDDLE;
	p.color = 0xFFFFFF;
	while (i < 5)
	{
		draw_polygone(e, p);
		p.r--;
		i++;
	}
}

void draw_plates(t_gui *e)
{
	int		i;
	t_poly	p;
	t_line	l;

	i = 0;
	while (i < PHILO)
	{
		l.x1 = MIDDLE + (WIDTH / 4.1) * cos(2 * PI * i / PHILO);
		l.y1 = MIDDLE + (WIDTH / 4.1) * sin(2 * PI * i / PHILO);
		l.x2 = MIDDLE + (WIDTH / 4.1) * cos(2 * PI * (i + 1) / PHILO);
		l.y2 = MIDDLE + (WIDTH / 4.1) * sin(2 * PI * (i + 1) / PHILO);
		p.r = WIDTH / 25;
		p.midx = (l.x1 + l.x2) / 2;
		p.midy = (l.y1 + l.y2) / 2;
		p.color = which_sticks(&e->philosophers[i]);
		draw_circle(e, p);
		i++;
	}
}

void draw_lifes(t_gui *e)
{
	int		i;
	t_poly	p;
	t_line	l;

	i = 0;
	while (i < PHILO)
	{
		l.x1 = MIDDLE + (WIDTH / 4.1) * cos(2 * PI * i / PHILO);
		l.y1 = MIDDLE + (WIDTH / 4.1) * sin(2 * PI * i / PHILO);
		l.x2 = MIDDLE + (WIDTH / 4.1) * cos(2 * PI * (i + 1) / PHILO);
		l.y2 = MIDDLE + (WIDTH / 4.1) * sin(2 * PI * (i + 1) / PHILO);
		p.midx = ((l.x1 + l.x2) / 2) - 5;
		p.midy = ((l.y1 + l.y2) / 2) - 12;
		pthread_mutex_lock(&e->philosophers[i].lock);
		mlx_string_put(e->mlx, e->win, p.midx, p.midy, 0xFFFFFF,
			ft_itoa(e->philosophers[i].life));
		pthread_mutex_unlock(&e->philosophers[i].lock);
		i++;
	}
}

void draw_polygone(t_gui *e, t_poly p)
{
	int		i;
	t_line	l;

	i = 0;
	while (i < p.n)
	{
		l.x1 = p.midx + p.r * cos(2 * PI * i / p.n);
		l.y1 = p.midy + p.r * sin(2 * PI * i / p.n);
		l.x2 = p.midx + p.r * cos(2 * PI * (i + 1) / p.n);
		l.y2 = p.midy + p.r * sin(2 * PI * (i + 1) / p.n);
		l.color = p.color;
		ft_draw_line(e, l);
		i++;
	}
}

void draw_sticks(t_gui *e)
{
	int i;
	int state;

	i = 0;
	while (i < PHILO)
	{
		draw_stick(e, i * 4, 0x000000);
		state = which_sticks(&e->philosophers[i]);
		if (state == LEFT)
		{
			draw_stick(e, i * 4 + 1, 0x0000FF); // left
			draw_stick(e, i * 4 + 3, 0x000000); // right
		}
		else if (state == RIGHT)
		{
			check_middle_left(e, i - 1, i);
			draw_stick(e, i * 4 + 1, 0x000000); // left
			draw_stick(e, i * 4 + 3, 0x0000FF); // right
		}
		else if (state == BOTH)
		{
			draw_stick(e, i * 4 + 1, 0xFF0000); // left
			draw_stick(e, i * 4 + 3, 0xFF0000); // right
		}
		else
		{
			check_middle_left(e, i - 1, i);
			draw_stick(e, i * 4 + 1, 0x000000); // left
			draw_stick(e, i * 4 + 3, 0x000000); // right
		}
		i++;
	}
}

void check_middle_left(t_gui *e, int i, int i2)
{
	if (i == -1)
	{
		i = PHILO - 1;
	}
	if (which_sticks(&e->philosophers[i]) == LEFT || which_sticks(&e->philosophers[i]) == NONE)
	{
		draw_stick(e, i2 * 4, 0x00FF00);
	}
}

void draw_stick(t_gui *e, int i, int color) {
	t_line	l;

	l.x1 = MIDDLE + (WIDTH / 4) * cos(2 * PI * i / (PHILO * 4));
	l.y1 = MIDDLE + (WIDTH / 4) * sin(2 * PI * i / (PHILO * 4));
	l.x2 = MIDDLE + (WIDTH / 5.2) * cos(2 * PI * i / (PHILO * 4));
	l.y2 = MIDDLE + (WIDTH / 5.2) * sin(2 * PI * i / (PHILO * 4));
	l.color = color;
	ft_draw_line(e, l);
}

void draw_circle(t_gui *e, t_poly p)
{
	double i_rad;
	int color;

	if (p.color == LEFT || p.color == RIGHT)
		color = 0x0000FF;
	else if (p.color == BOTH)
		color = 0xFF0000;
	else
		color = 0x00FF00;
    for (int i = 0; i < 360; i++)
    {
		i_rad = 2 * PI * i / 360;
		ft_px(e, (int)(p.midx + (p.r * cos(2 * (double)i_rad))),
			(int)(p.midy + (p.r * sin(2 * (double)i_rad))), color);
    }
}
