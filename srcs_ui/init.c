/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cemonet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 16:36:31 by cemonet           #+#    #+#             */
/*   Updated: 2017/05/23 18:39:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

int		test(void *ptr)
{
	t_gui	*e;
	bool	res;

	e = (t_gui *)ptr;
	if ((res = all_alive(e->philosophers)) && time(NULL) -
			e->philosophers[0].initial_timestamp < TIMEOUT)
	{
		draw_gui(e);
		mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
		draw_lifes(e);
	}
	else
	{
		; /* PRINT IT's TIME TO DANCE */
	}
	return (1);
}

void	ft_init_gui(t_gui *e)
{
	e->mlx = mlx_init();
	if (e->mlx == NULL)
		exit(1);
	e->win = mlx_new_window(e->mlx, WIDTH, HEIGHT, "42 - cemonet / mmeisson - Philosophers");
	if (e->win == NULL)
		exit(1);
	e->img = mlx_new_image(e->mlx, WIDTH, HEIGHT);
	if (e->img == NULL)
		exit(1);
	e->px = mlx_get_data_addr(e->img, &e->bpp, &e->sl, &e->ed);
	mlx_loop_hook(e->mlx, test, (void *)e);
}
