/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cemonet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 14:06:04 by cemonet           #+#    #+#             */
/*   Updated: 2016/01/20 19:55:57 by cemonet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void	ft_draw_line(t_gui *e, t_line l)
{
	int		dx;
	int		dy;

	dy = 0;
	if ((dx = l.x2 - l.x1) != 0)
		ft_draw_line2(e, l, dx, dy);
	else
	{
		if ((dy = l.y2 - l.y1) != 0)
		{
			if (dy > 0)
				while ((l.y1 = l.y1 + 1) != l.y2)
					ft_px(e, l.x1, l.y1, l.color);
			else
				while ((l.y1 = l.y1 - 1) != l.y2)
					ft_px(e, l.x1, l.y1, l.color);
		}
	}
}

void	ft_draw_line2(t_gui *e, t_line l, int dx, int dy)
{
	if (dx > 0)
	{
		if ((dy = l.y2 - l.y1) != 0)
		{
			if (dy > 0)
				ft_cadran1(e, l, dx, dy);
			else
				ft_cadran4(e, l, dx, dy);
		}
		else
			while ((l.x1 = l.x1 + 1) != l.x2)
				ft_px(e, l.x1, l.y1, l.color);
	}
	else
	{
		if ((dy = l.y2 - l.y1) != 0)
			ft_draw_line3(e, l, dx, dy);
		else
			while ((l.x1 = l.x1 - 1) != l.x2)
				ft_px(e, l.x1, l.y1, l.color);
	}
}

void	ft_draw_line3(t_gui *e, t_line l, int dx, int dy)
{
	if (dy > 0)
		ft_cadran2(e, l, dx, dy);
	else
		ft_cadran3(e, l, dx, dy);
}
