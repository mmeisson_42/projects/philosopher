/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_cadran.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cemonet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 14:15:41 by cemonet           #+#    #+#             */
/*   Updated: 2016/01/20 18:17:48 by cemonet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void	ft_cadran1(t_gui *e, t_line l, int dx, int dy)
{
	int err;

	if (dx >= dy)
	{
		err = dx;
		dx = err * 2;
		dy *= 2;
		while ((l.x1++) != l.x2)
		{
			ft_px(e, l.x1, l.y1, l.color);
			if ((err -= dy) < 0)
			{
				l.y1++;
				err += dx;
			}
		}
	}
	else
		ft_octant2(e, l, dx, dy);
}

void	ft_cadran4(t_gui *e, t_line l, int dx, int dy)
{
	int err;

	if (dx >= -dy)
	{
		err = dx;
		dx = err * 2;
		dy *= 2;
		while ((l.x1++) != l.x2)
		{
			ft_px(e, l.x1, l.y1, l.color);
			if ((err += dy) < 0)
			{
				l.y1--;
				err += dx;
			}
		}
	}
	else
		ft_octant7(e, l, dx, dy);
}

void	ft_cadran2(t_gui *e, t_line l, int dx, int dy)
{
	int err;

	if (-dx >= dy)
	{
		err = dx;
		dx = err * 2;
		dy *= 2;
		while ((l.x1--) != l.x2)
		{
			ft_px(e, l.x1, l.y1, l.color);
			if ((err += dy) >= 0)
			{
				l.y1++;
				err += dx;
			}
		}
	}
	else
		ft_octant3(e, l, dx, dy);
}

void	ft_cadran3(t_gui *e, t_line l, int dx, int dy)
{
	int err;

	if (dx <= dy)
	{
		err = dx;
		dx = err * 2;
		dy *= 2;
		while ((l.x1--) != l.x2)
		{
			ft_px(e, l.x1, l.y1, l.color);
			if ((err -= dy) >= 0)
			{
				l.y1--;
				err += dx;
			}
		}
	}
	else
		ft_octant6(e, l, dx, dy);
}
