/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_octant.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cemonet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 14:35:29 by cemonet           #+#    #+#             */
/*   Updated: 2017/05/20 09:30:50 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void	ft_octant2(t_gui *e, t_line l, int dx, int dy)
{
	int err;

	err = dy;
	dy = err * 2;
	dx *= 2;
	while ((l.y1++) != l.y2)
	{
		ft_px(e, l.x1, l.y1, l.color);
		if ((err -= dx) < 0)
		{
			l.x1++;
			err += dy;
		}
	}
}

void	ft_octant7(t_gui *e, t_line l, int dx, int dy)
{
	int err;

	err = dy;
	dy = err * 2;
	dx *= 2;
	while ((l.y1--) != l.y2)
	{
		ft_px(e, l.x1, l.y1, l.color);
		if ((err += dx) > 0)
		{
			l.x1++;
			err += dy;
		}
	}
}

void	ft_octant3(t_gui *e, t_line l, int dx, int dy)
{
	int err;

	err = dy;
	dy = err * 2;
	dx *= 2;
	while ((l.y1++) != l.y2)
	{
		ft_px(e, l.x1, l.y1, l.color);
		if ((err += dx) <= 0)
		{
			l.x1--;
			err += dy;
		}
	}
}

void	ft_octant6(t_gui *e, t_line l, int dx, int dy)
{
	int err;

	err = dy;
	dy = err * 2;
	dx *= 2;
	while ((l.y1--) != l.y2)
	{
		ft_px(e, l.x1, l.y1, l.color);
		if ((err -= dx) >= 0)
		{
			l.x1--;
			err += dy;
		}
	}
}
