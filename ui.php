#!/usr/bin/php -n
<?php
$size = 600;
$n = 7;
$p = M_PI;

// 0 = mange
// 1 = dors
// 2 = reflechi
$philo = array(0, 1, 2, 0, 2, 1, 1);

$image = imagecreate($size, $size);

$white = imagecolorallocate($image, 255, 255, 255);
$black = imagecolorallocate($image, 0, 0, 0);
$red = imagecolorallocate($image, 255, 0, 0);
$orange = imagecolorallocate($image, 255, 153, 0);
$green = imagecolorallocate($image, 0, 255, 0);

$r = $size / 3;
for ($i = 0; $i < $n; $i++) {
	$x1 = ($size / 2) + $r * cos(2 * $p * $i / $n);
	$y1 = ($size / 2) + $r * sin(2 * $p * $i / $n);
	$x2 = ($size / 2) + $r * cos(2 * $p * ($i + 1) / $n);
	$y2 = ($size / 2) + $r * sin(2 * $p * ($i + 1) / $n);
	imageline($image, $x1, $y1, $x2, $y2, $black);
}

$r = $size / 4.1;
for ($i = 0; $i < $n; $i++) {
	$x1 = ($size / 2) + $r * cos(2 * $p * $i / $n);
	$y1 = ($size / 2) + $r * sin(2 * $p * $i / $n);
	$x2 = ($size / 2) + $r * cos(2 * $p * ($i + 1) / $n);
	$y2 = ($size / 2) + $r * sin(2 * $p * ($i + 1) / $n);

	if ($philo[$i] == 0) {
		$color = $red;
	} else if ($philo[$i] == 1) {
		$color = $green;
	} else {
		$color = $orange;
	}

	$midx = ($x1 + $x2) / 2;
	$midy = ($y1 + $y2) / 2;
	$r2 = $size / 25;
	for ($j = 0; $j < 360; $j++) {
		$rx1 = $midx + $r2 * cos(2 * $p * $j / 360);
		$ry1 = $midy + $r2 * sin(2 * $p * $j / 360);
		$rx2 = $midx + $r2 * cos(2 * $p * ($j + 1) / 360);
		$ry2 = $midy + $r2 * sin(2 * $p * ($j + 1) / 360);
		imageline($image, $rx1, $ry1, $rx2, $ry2, $color);
	}
}

for ($i = 0; $i < $n * 4; $i++) {
	if (($i + 1) % 2 == 0) {
		$nb = ceil(($i + 1) / 4) - 1;
		$philo_state = $philo[$nb];
		if ($philo_state == 0) {
			$r_out = $size / 4.0;
			$r_in = $size / 5.2;
			$x1 = ($size / 2) + $r_out * cos(2 * $p * $i / ($n * 4));
			$y1 = ($size / 2) + $r_out * sin(2 * $p * $i / ($n * 4));
			$x2 = ($size / 2) + $r_in * cos(2 * $p * $i / ($n * 4));
			$y2 = ($size / 2) + $r_in * sin(2 * $p * $i / ($n * 4));
			imageline($image, $x1, $y1, $x2, $y2, $red);
		}
		$philo_right = $philo[$nb - 1];
		$philo_left = $philo[$nb + 1];
		if ($philo_right == -1) {
			$philo_right = count($philo) - 1;
		}
		if ($philo_left == count($philo)) {
			$philo_left = 0;
		}
		$philo_left_state = $philo[$philo_left];
		$philo_right_state = $philo[$philo_right];
		if ($philo_state == 2 && (((($i + 1) / 2) % 2 == 0 && $philo_left_state == 1) || ((($i + 1) / 2) % 2 == 1 && $philo_right_state == 1))) {
			$r_out = $size / 4.0;
			$r_in = $size / 5.2;
			$x1 = ($size / 2) + $r_out * cos(2 * $p * $i / ($n * 4));
			$y1 = ($size / 2) + $r_out * sin(2 * $p * $i / ($n * 4));
			$x2 = ($size / 2) + $r_in * cos(2 * $p * $i / ($n * 4));
			$y2 = ($size / 2) + $r_in * sin(2 * $p * $i / ($n * 4));
			imageline($image, $x1, $y1, $x2, $y2, $orange);
		}
	}
	if ($i % 4 == 0) {
		$left_philo = $i / 4;
		$right_philo = $left_philo - 1;
		if ($right_philo == -1) {
			$right_philo = count($philo) - 1;
		}
		$left_philo_state = $philo[$left_philo];
		$right_philo_state = $philo[$right_philo];
		if ($left_philo_state == 1 && $right_philo_state == 1) {
			$r_out = $size / 4.0;
			$r_in = $size / 5.2;
			$x1 = ($size / 2) + $r_out * cos(2 * $p * $i / ($n * 4));
			$y1 = ($size / 2) + $r_out * sin(2 * $p * $i / ($n * 4));
			$x2 = ($size / 2) + $r_in * cos(2 * $p * $i / ($n * 4));
			$y2 = ($size / 2) + $r_in * sin(2 * $p * $i / ($n * 4));
			imageline($image, $x1, $y1, $x2, $y2, $green);
		}
	}
}

$file = "philo.png";
imagepng($image, $file);
shell_exec("open ".$file);
?>
